/**
 * School Days.
 * draw few kind of random shapes in the organized location.
 * 
 * @author @deconbatch
 * @version 0.1
 * Processing 3.2.1
 * created:2019.10.12
 */

void setup() {

  size(980, 980);
  colorMode(HSB, 360, 100, 100, 100);
  smooth();
  noLoop();

}

void draw() {

  int   imgMax  = 3;  // draw 3 images
  float baseHue = random(360.0);

  for (int imgCnt = 1; imgCnt <= imgMax; ++imgCnt) {

    baseHue += 80.0;

    // background image
    blendMode(BLEND);
    background(baseHue % 360.0, 10.0, 90.0, 100.0);
    noStroke();
    fill(baseHue % 360.0, 90.0, 20.0, 100.0);
    beginShape();
    vertex(width, 0.0);
    vertex(width, height);
    vertex(0.0, height);
    endShape(CLOSE);
    fill(baseHue % 360.0, 50.0, 30.0, 40.0);
    beginShape();
    vertex(0.0, 0.0);
    vertex(width, height);
    vertex(width, 0.0);
    endShape(CLOSE);

    // foreground image
    drawReels(baseHue);

    // casing
    blendMode(BLEND);
    casing(baseHue);
    
    saveFrame("frames/" + String.format("%04d", imgCnt) + ".png");

  }

  exit();

}

private void drawReels(float _baseHue) {

  int   cellCnt    = floor(random(10.0, 20.0));
  int   plotCntMax = cellCnt * 400;
  float plotScale  = 0.0001;
  float lineWeight = map(cellCnt, 10, 20, 2.5, 1.0) * map(plotCntMax, 4000, 8000, 2.0, 1.0);
  float offW       = 0.05;
  float offH       = 0.05;
  float divW       = (1.0 - offW * 2.0) / cellCnt;
  float divH       = (1.0 - offH * 2.0) / cellCnt;
  float tuneH      = -0.01 * lineWeight;  // it doesn't work well

  int shapeCnt = 5;
  float[] shapeFactor = new float[shapeCnt];
  for (int i = 0; i < shapeCnt; i++) {
    shapeFactor[i] = sin((_baseHue + i * 0.1) % PI) * 0.15 + 0.3; // magic spell
  }

  for (int x = 0; x < cellCnt; x++) {
    float xInit = offW + (x + 0.5) * divW;
      
    for (int y = 0; y < cellCnt; y++) {
      float yInit = offH + (y + 0.5) * divH + tuneH;
        
      float xPrev = 0.0;
      float yPrev = 0.0;
      float xCurr = 0.0;
      float yCurr = 0.0;

      // draw a random shape in the fixed location
      int shapeIndex = floor(random(shapeCnt));
      for (int plotCnt = 0; plotCnt < plotCntMax; ++plotCnt) {
        float plotRatio = map(plotCnt, 0, plotCntMax, 0.0, 1.0);

        xPrev += plotScale * cos(TWO_PI * shapeFactor[shapeIndex]);
        yPrev += plotScale * sin(TWO_PI * shapeFactor[shapeIndex]);
        xCurr += plotScale * cos(TWO_PI * xPrev * cellCnt);
        yCurr += plotScale * sin(TWO_PI * yPrev * cellCnt);

        float eHue = _baseHue + floor(((xInit * yInit) * 1000.0) % 7.0) * 10.0 + plotRatio * 30.0;
        float eSat = sin(PI * plotRatio) * 50.0;
        float eBri = abs(sin((TWO_PI + PI) * plotRatio)) * 5.0;
        float eAlp = map(sin(TWO_PI * plotRatio), -1.0, 1.0, 100.0, 50.0);
        float eSiz = pow(sin(3.0 * TWO_PI * plotRatio), 2) * lineWeight;

        // To invert brightness.
        if (xCurr + yCurr + xInit + yInit < 1.0) {
          blendMode(DIFFERENCE);
          fill((eHue + 150.0) % 360.0, eSat, eBri, eAlp);
          ellipse((xInit + xCurr) * width, (yInit + yCurr) * height, eSiz, eSiz);
        } else {
          blendMode(SCREEN);
          fill(eHue % 360.0, eSat, eBri, eAlp);
          ellipse((xInit + xCurr) * width , (yInit + yCurr) * height, eSiz, eSiz);
        }
        
      }
    }
  }
}

/**
 * casing : draw fancy casing
 */
private void casing(float _baseHue) {
  fill(_baseHue % 360.0, 100.0, 20.0, 0.0);
  strokeWeight(50.0);
  stroke(0.0, 0.0, 0.0, 100.0);
  rect(0.0, 0.0, width, height);
  strokeWeight(40.0);
  stroke(0.0, 0.0, 100.0, 100.0);
  rect(0.0, 0.0, width, height);
  noStroke();
  noFill();
}
